![forthebadge](https://forthebadge.com/images/badges/made-with-javascript.svg)
![forthebadge](https://forthebadge.com/images/badges/built-with-love.svg)
![forthebadge](https://forthebadge.com/images/badges/60-percent-of-the-time-works-every-time.svg)

# O que é o Crunchyroll  iFrame Player?
É uma extensão que permite assistir todo conteúdo da Crunchyroll de graça e sem necessidade de possuir uma conta no site.

# Download
Você pode encontrar a última versão disponível [aqui](https://bitbucket.org/Maycongc/crp-iframe-player/downloads/Crunchyroll_Premium.zip) 

# Como instalar? (PC)

 1. Faça o download do arquivo "Crunchyroll_Premium.zip", e extraia-o  
 ![Extraindo arquivo baixado](https://bitbucket.org/Maycongc/crp-iframe-player/raw/f258482533604f71da7d8f4163de653eb52f2bfd/Screenshots/instalacao-3.png)
 2. Abra o seu navegador (Apenas navegadores baseados em Chromium)
 3. Clique nos três pontos que estão na parte superior direita da janela do navegador. Vá em Mais Ferramentas, e clique em "Extensões"  
 ![Abrindo gerenciador de extensões](https://bitbucket.org/Maycongc/crp-iframe-player/raw/f258482533604f71da7d8f4163de653eb52f2bfd/Screenshots/instalacao-1.png)
 4. Habilite o "Modo programador", e então clique em "Carregar expandida"  
 ![Habilitando modo programador, e carregando extensão](https://bitbucket.org/Maycongc/crp-iframe-player/raw/f258482533604f71da7d8f4163de653eb52f2bfd/Screenshots/instalacao-2.png)
 5. Escolha a pasta que extraímos no começo do tutorial, e clique em "Selecionar pasta"  
 ![Carregando extensão para o navegador](https://bitbucket.org/Maycongc/crp-iframe-player/raw/f258482533604f71da7d8f4163de653eb52f2bfd/Screenshots/instalacao-4.png)
 6. Se você fez tudo certo, você deverá ver algo assim no seu navegador  
 ![Detalhes da extensão instalada](https://bitbucket.org/Maycongc/crp-iframe-player/raw/f258482533604f71da7d8f4163de653eb52f2bfd/Screenshots/instalacao-5.png)
 7. Agora é só assistir 😉

Obrigado por utilizar.
