let HTML = document.documentElement.innerHTML;

// function que pega algo dentro dentro do html.
const pegaString = (str, first_character, last_character) => {
	if(str.match(first_character + "(.*)" + last_character) !== null) {
		new_str = str.match(first_character + "(.*)" + last_character)[1].trim()
		return(new_str)
	}
}
// function que mudar o player para um mais simples.
const importPlayer = () => {
		console.log("[CR Premium] Removendo player da Crunchyroll...");
		document.querySelector('#showmedia_video_player').remove();

		console.log("[CR Premium] Pegando dados da stream...");
		let video_config_media = JSON.parse(pegaString(HTML, "vilos.config.media = ", ";"));

		console.log("[CR Premium] Adicionando o jwplayer...");
		ifrm = document.createElement("iframe");
		ifrm.setAttribute("id", "frame"); 
		ifrm.setAttribute("src", "https://crp-iframe-player.herokuapp.com/"); 
		ifrm.setAttribute("width","100%");
		ifrm.setAttribute("height","100%");
		ifrm.setAttribute("frameborder","0");
		ifrm.setAttribute("scrolling","no");
		ifrm.setAttribute("allowfullscreen","allowfullscreen");
		ifrm.setAttribute("allow","autoplay; encrypted-media *");

		if(document.querySelector("#showmedia_video_box")) {
			document.querySelector("#showmedia_video_box").appendChild(ifrm);
		} else {
			document.querySelector("#showmedia_video_box_wide").appendChild(ifrm);
		}
		
		ifrm.onload = () => {
			ifrm.contentWindow.postMessage({
           		'video_config_media': [JSON.stringify(video_config_media)],
           		'lang': [pegaString(HTML, 'LOCALE = "', '",')]
			}, "*");
		};

		// Estilizando título do episódio
		document.querySelector("#showmedia_about_name").style.color = "#F47521"
		document.querySelector("#showmedia_about_name").style.fontSize = "15px"
}

const remove = () => {
	// Remove Nota do topo sobre experimentar o premium
	if (document.querySelector(".freetrial-note")) 
		document.querySelector(".freetrial-note").remove();

	// Remove o a opção Premium do menu principals
	if (document.querySelector(".premium-link-text")) 
		document.querySelector(".premium-link-text").remove();

	// Remove nota sobre premium no calendario
	if (document.querySelector(".premium-message")) 
		document.querySelector(".premium-message").remove();

	// Remove avisos q o video nn pode ser visto
	if(document.querySelector(".showmedia-trailer-notice"))
		document.querySelector(".showmedia-trailer-notice").remove();

	// Remove sugestão de inscrever-se para o trial gratuito
	if(document.querySelector("#showmedia_free_trial_signup"))
		document.querySelector("#showmedia_free_trial_signup").remove();

	// Remove footer premium do calendario
	if(document.querySelector(".simulcast-calendar-footer"))
		document.querySelector(".simulcast-calendar-footer").remove();
}

const swap = () => {
	if(document.querySelector(".welcome-block")){
		const news = document.querySelectorAll(".welcome-block")[0];
		const simulCast = document.querySelectorAll(".welcome-block")[1];
		const episodes = document.querySelectorAll(".welcome-block")[2];

		// Movendo epsódios novos para antes de notícias
		news.before(episodes);

		// Movendo Simulcast para antes de notícias
		news.before(simulCast);

		// noticias/simulcast/epsódios -> epsódios/noticias/simulcast -> epsódios/simulcast/noticias
	}
}


// function ao carregar pagina.
const onloadfunction = () => {
	// Mudando a ordem do conteúdo da homepage
	swap();

	// Removendo notas sobre o premium
	remove();

	// Carregar o player
	if(pegaString(HTML, "vilos.config.media = ", ";"))
		importPlayer();
}
document.addEventListener("DOMContentLoaded", onloadfunction());
